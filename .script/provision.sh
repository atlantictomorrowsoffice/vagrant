#! /bin/bash
######################### Add necessary packages for the VM ############################

# Set up ssh keys
dos2unix /vagrant/.script/*.sh
sudo chmod +x /vagrant/.script/*.sh
/vagrant/.script/copykeys.sh
 

# Install and configure docker
/vagrant/.script/setupdocker.sh

# Setup App
/vagrant/.script/setupapp.sh

# Setup Vagrant for local Development (in addition to Docker)
/vagrant/.script/setupdev.sh

# Setup Jenkins for CI - Un-comment if you need Jenkins
#/vagrant/.script/setupjenkins.sh

# Setup GUI enviornment - Un-comment if you need a gui
#/vagrant/.script/setupgui.sh


# Set up bash aliases to be used inside vagrant
dos2unix /vagrant/.script/.bash_aliases
ln -s /vagrant/.script/.bash_aliases /home/vagrant/.bash_aliases
#cp /vagrant/.script/.bash_aliases /home/vagrant/.bash_aliases
source /home/vagrant/.bash_aliases

echo "Now run"
echo "source ~/.bash_aliases"