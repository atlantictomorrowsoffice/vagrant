#!/bin/bash
#========================     PROVISION VAGRANT BOX FOR DEVELOPMENT   ==================================#
sudo apt-get update
sudo aptitude update
sudo aptitude -y upgrade

#Basic requirements
sudo apt-get install -y dos2unix xclip git curl
#sudo apt-get install rsync imagemagick xrdp cifs-utils

#Install and setup DNSMasq
sudo apt-get install dnsmasq
sudo sed 's/\#conf-dir\=\/etc\/dnsmasq.d/conf-dir\=\/etc\/dnsmasq.d/g' /etc/dnsmasq.conf
sudo mkdir -p /etc/dnsmasq.d
sudo cp /vagrant/.script/domains.conf /etc/dnsmasq.d && sudo service dsnmasq restart

# Install Provisoiner
git clone git@bitbucket.org:activeobjectsllc/provisioner.git /vagrant
echo 'source /vagrant/provisioner/scripts/bash_aliases' >> /home/vagrant/.bashrc

# Package Installers
/vagrant/provisioner/scripts/installers/php
/vagrant/provisioner/scripts/installers/composer
/vagrant/provisioner/scripts/installers/node
/vagrant/provisioner/scripts/installers/laravel

sudo apt-get install mysql-client -y

# Uninstall Apache
sudo service apache2 stop
sudo apt-get remove apache2* -y

#Add more Inodes
#sudo sed 's/\# Log Martian Packets/fs.inotify.max_user_watches = 524288/g' /etc/sysctl.conf
#sudo sysctl -p

#Change SSH mode to non strict
#sudo sed -i 's/StrictModes yes/StrictModes no/g' /etc/ssh/sshd_config && sudo service ssh restart