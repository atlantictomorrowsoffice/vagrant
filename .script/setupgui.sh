#!/bin/bash
#========================     PROVISION OPTIONAL COMPONENTS BOX    ==================================#

#Need A desktop
sudo apt-get install lubuntu-desktop -y
sudo apt-get remove light-locker -y
sudo apt-get install -y xscreensaver virtualbox-guest-x11

(cd /home/vagrant && rm -Rf Documents/ Music/ Pictures/ Public/ Templates/ Videos/)
# Need flash and java in browser?
#sudo apt-get install ubuntu-restricted-extras

# Need a terminal
sudo apt-get install -y terminator

# Check to see if the provisioner is installed
if [ -d "/vagrant/provisioner" ]; then

	#Sublime Text
	/vagrant/provisioner/scripts/installers/sublimetext	

	#Java
	#/vagrant/provisioner/site-php/scripts/installers/java.sh

	#Composer
	#/vagrant/provisioner/site-php/scripts/installers/composer.sh
			
	#Apache SSL
	#/vagrant/provisioner/site-php/scripts/installers/apache_ssl.sh

	#Postgres
	#sudo apt-get install -y php5-pgsql

	#PHP Unit
	#/vagrant/provisioner/site-php/scripts/installers/phpunit.sh


	#PHP Storm
	#/vagrant/provisioner/site-php/scripts/installers/phpstorm.sh

	#Wine
	#/vagrant/provisioner/site-php/scripts/installers/wine.sh

	#Chrome
	#/vagrant/provisioner/site-php/scripts/installers/chrome.sh

	#Smart Git
	#/vagrant/provisioner/site-php/scripts/installers/smartgit.sh
fi;
