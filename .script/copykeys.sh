#!/bin/bash
#========================     REINSTALL PERSONAL KEYS    ==================================#

sudo cp /vagrant/.sshkeys/id_rsa* /home/vagrant/.ssh/
sudo chown vagrant:vagrant /home/vagrant/.ssh/id_rsa*
sudo chmod 600 /home/vagrant/.ssh/id_rsa* 
su - vagrant -c "eval `ssh-agent -s`"
su - vagrant -c "ssh-add /home/vagrant/.ssh/id_rsa"
#su - vagrant -c sudo ssh-add /home/vagrant/.ssh/id_rsa
echo ""