#!/bin/bash
#========================  INSTALL GENESIS  ==================================#

# Setup Genesis
sudo ssh-keyscan -H bitbucket.org >> /home/vagrant/.ssh/known_hosts

# Checkout app repository
su - vagrant -c "mkdir ~/work"
su - vagrant -c "git clone --recursive git@bitbucket.org:activeobjectsllc/laradock.git ~/work/docker"

# Setup enviornment and build app containers
su - vagrant -c "ln -s ~/work/docker/.env.local ~/work/docker/.env"
su - vagrant -c "cd ~/work/docker && docker-compose build app"

# Deploy the App using Envoy
#su - vagrant -c "cd ~/app/docker && \
#docker-compose run app su laradock -c '~/.composer/vendor/bin/envoy run deploy'"

#su - vagrant -c "cd /vagrant/genesis/docker &&
#docker-compose run app /home/laradock/.composer/vendor/bin/envoy run deploy"
