# Aliases for Vagrant to be used in conjunction with the docker/laradock setup in /vagrant/genesis/docker

alias up="dc up -d app"
alias myup="dc up -d mysql"
alias down="dc down"
alias build="dc build app"

alias nup="dc up -d nginx"
alias wbash="dc run workspace bash"
alias mybash="docker exec -it local_mysql_1 mysql -uroot"

alias serve="php artisan serve --host=0.0.0.0"

#alias abash="dc run app bash"
#alias test="dc run app test"


# Delete all containers
alias drm='docker rm $(docker ps -a -q)'
# Delete all images and DATA!
alias dri='docker rmi $(docker images -q) -f'


#(cd ~/app/docker && docker-compose "$@")
function dc() {
   (cd ~/work/docker && docker-compose "$@")
}