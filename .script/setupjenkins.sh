#!/bin/bash
#========================  INSTALL AND CONFIGURE JENKINS  ==================================#

sudo ssh-keyscan -H bitbucket.org >> /home/vagrant/.ssh/known_hosts

# Clone Jenkins configuration
su - vagrant -c "git clone git@bitbucket.org:atlantictomorrowsoffice/jenkins.git /vagrant/jenkins"

# Copy Jenkins SSH Keys to jenkins_home
su - vagrant -c "cp /vagrant/.sshkeys/jenkins/id_rsa* /vagrant/jenkins/jenkins_home/.ssh"

# build the app's docker containers
su - vagrant -c " cd /vagrant/genesis/docker && \
docker-compose build"

# Setup jenkins_home alias in order to make running builds with docker in docker work
sudo ln -s /vagrant/jenkins/jenkins_home /jenkins_home
