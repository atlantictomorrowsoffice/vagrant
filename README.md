# Vagrant set up for Genesis #

## Getting Set Up ##
1. First install [Virtualbox](http://www.virtualbox.org)
1. Then install [Vagrant](http://www.vagrantup.com)
1. Next clone this repo
```bash
# Assuming you use a projects folder
git clone git@bitbucket.org:atlantictomorrowsoffice/vagrant.git ~/projects
cd ~/projects/vagrant
vagrant up --provision
```
1. Copy your existing keys to ~/projects/vagrant/.sshkeys
1. Start the vagrant box
```bash
cd ~/projects/vagrant
vagrant up --provision
```
1. Upon completion of the build ssh into the box and run 
```bash
vagrant ssh     # ssh into the box OR use your favorite ssh client
up              # start the preconfigured docker containers
wbash           # start the workspace bash prompt
```
1. visit [http://localhost](http://localhost) or [http://localhost:8080](http://localhost:8080)
user: admin@admin.com
pass: admin

## Useful Aliases to be run inside vagrant ##
1. You can find useful aliases in the ./script/bash_aliases file. These commands can be run
 inside vagrant and *NOT* inside containers
```bash
up              # Start the project
down            # Stop the project
wbash           # Enter the workspace bash prompt in order to run laravel commands

dri             # Completely Remove all images and volumes and destroys all images
```

# Extras
## Jenkins
If you need to have jenkins set up uncomment the following line in `.script/provision.sh`
```
/vagrant/.script/setupjenkins.sh
```

Running Jenkins
```bash
cd /vagrant/jenkins
docker-compose up -d
```

Jenkins: [http://localhost:8090](http://localhost:8090)
admin / admin
