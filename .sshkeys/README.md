# Setup SSH Keys for Git  #

Prerequisite: Copy your ssh key files to this folder ie. id_rsa and id_rsa.pub. 
The VM will install these files which will enable you to run git from the command line 
without having to enter your password each time.

For additional help on setting up SSH Keys, visit https://help.github.com/articles/generating-ssh-keys