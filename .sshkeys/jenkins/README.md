# Setup SSH Keys for Jenkins #

Prerequisite: Copy your ssh key files to this folder ie. id_rsa and id_rsa.pub. 
The VM will install these files for Jenkins CI server

For additional help on setting up SSH Keys, visit https://help.github.com/articles/generating-ssh-keys