# -*- mode: ruby -*-
# vi: set ft=ruby :
# Documentation https://docs.vagrantup.com

box         = 'bento/ubuntu-16.04'
#boxver      = '0.1.1'
hostname    = 'vagrant'
domain      = 'activeobjects.net'
privip      = '192.168.0.8'
ram         = '4096'
cores       = '1'

Vagrant.configure(2) do |config|  
  
  # In case you have vagrant ssh problems uncomment these lines
  #config.ssh.username = "vagrant"
  #config.ssh.password = "vagrant"
  
  config.vm.box = box
  config.vm.boot_timeout = 300                    # Time out for box tarts
  config.vm.host_name = hostname + '.' + domain   # Host Name
  #config.vm.box_check_update = false             # Stop box updates
  #config.ssh.insert_key = false                  # SSH config to avoid key problems

  config.vm.network "private_network", ip: privip # Ip address
  #config.vm.network "public_network"

  # Foward your host machine ports on parent to guest
  config.vm.network "forwarded_port", guest: 80, host: 8080, auto_correct: true      # nginx http
  config.vm.network "forwarded_port", guest: 8000, host: 8000, auto_correct: true    # artisan http
  config.vm.network "forwarded_port", guest: 3000, host: 3000, auto_correct: true    # mix browser-sync
  config.vm.network "forwarded_port", guest: 3001, host: 3001, auto_correct: true    # mix browser-sync external
  config.vm.network "forwarded_port", guest: 8090, host: 8090, auto_correct: true    # Jenkins
  config.vm.network "forwarded_port", guest: 50000, host: 50000, auto_correct: true  # Jenkins tasks
  config.vm.network "forwarded_port", guest: 3306, host: 3306, auto_correct: true    # MySql
  config.vm.network "forwarded_port", guest: 6379, host: 6379, auto_correct: true    # Redis
  config.vm.network "forwarded_port", guest: 4040, host: 4040, auto_correct: true    # Ngrok

  # Shared folders - If you have problems mounting the shared folder try - vagrant plugin install vagrant-nfs_guest
  config.vm.synced_folder ".", "/vagrant"

  # Share Folders - Windows Parent
  #config.vm.synced_folder "www", "/var/www", id: "vagrant-www", type: nfs, group: 'www-data', owner: 'vagrant', mount_options: ["dmode=777", "fmode=764"]  
  
  # Share Folders - Mac Parent
  #config.vm.synced_folder "www", "/var/www", id: "vagrant-www", type: sharetype, mount_options: ["dmode=777", "fmode=764"]  

  config.vm.provider "virtualbox" do |vb|    
    vb.gui = true             # Display the VirtualBox GUI when booting the machine
    vb.memory = ram           # Customize the amount of memory on the VM
    vb.cpus = cores           # Customize the number of cores in the VM recommended 1
    vb.customize ["modifyvm", :id, "--clipboard", "bidirectional", "--usbehci", "off", "--vram", "9"] # Bi-Directional clipboard, USB, video ram
    vb.customize ["modifyvm", :id, "--cableconnected1", "on"]
    # If you are having network problems you may want to uncomment the following on windows parents    
    #vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    #vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
  end
  
  # Remove windows line endings from files
  config.vm.provision "shell", inline: <<-SHELL
    sudo apt-get install -y dos2unix 
    sudo dos2unix /vagrant/.script/*.sh
    sudo chmod +x /vagrant/.script/*.sh
    sudo /vagrant/.script/copykeys.sh
  SHELL
  
  config.vm.provision "shell", inline: "/bin/bash /vagrant/.script/provision.sh", privileged: true

  # On mac if you want to use port 80 on the host. Requires vagrant triggers
  # vagrant plugin install vagrant-triggers 
  #config.trigger.after [:provision, :up, :reload] do
  #    system('echo "
  # rdr pass on lo0 inet proto tcp from any to 127.0.0.1 port 80 -> 127.0.0.1 port 8080  
  # rdr pass on lo0 inet proto tcp from any to 127.0.0.1 port 443 -> 127.0.0.1 port 8443  
  # " | sudo pfctl -f - > /dev/null 2>&1; echo "==> Fowarding Ports: 80 -> 8080, 443 -> 8443"')  
  #end
  #config.trigger.after [:halt, :destroy] do
  #  system("sudo pfctl -f /etc/pf.conf > /dev/null 2>&1; echo '==> Removing Port Forwarding'")
  #end

  # Atlas Configuration Comment this line if you are not using atlas
  #config.vm.box_version = boxver

  #config.push.define "atlas" do |push|
  #   push.app = box
  #end
end
